var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
const path = require('path')
const vueLoaderConfig = require('./vue-loader.conf')
var outputFile = 'vuewyg'
var globalName = 'Vuewyg'
var UglifyJsPlugin = require('uglifyjs-webpack-plugin')
var CompressionPlugin = require("compression-webpack-plugin");

function resolve (dir) {
	return path.join(__dirname, '..', dir)
}
var config = require('../package.json')
module.exports = {
	entry: './src/index.js',
	resolve: {
		extensions: ['.js', '.vue', '.json','.coffee'],
		alias: {
			'vue$': 'vue/dist/vue.esm.js',
			'@': resolve('src'),
		},
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: vueLoaderConfig
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				include: [resolve('src'), resolve('test')]
			},      
			{
				test: /\.coffee$/,
				use: ['babel-loader','coffee-loader'],
				include: [resolve('src'), resolve('test')],
				exclude: /node_modules/
			},
		],
	},
	plugins: [
		new UglifyJsPlugin({ sourceMap: true }),
		new webpack.DefinePlugin({
			'VERSION': JSON.stringify(config.version),
		}),
		new ExtractTextPlugin(outputFile + '.css'),
		new CompressionPlugin({
			asset: "[path].gz[query]",
			algorithm: "gzip",
			test: /\.js$|\.css$|\.html$/,
			//threshold: 10240,
			//minRatio: 0
		})
	],
	 externals: {
		//'content-edit': 'content-edit',
		'vuetify': 'vuetify',
		'vue-resource': 'vue-resource',
		'vuex': 'vuex',
		'vue': 'vue',
		'axios': 'axios',
		'prismjs': 'prismjs',
		'vue-croppa': 'vue-croppa',
		'sanitize-html': 'sanitize-html',
		// Put external libraries like lodash here
		// With their global name
		// Example: 'lodash': '_'
	},
}
