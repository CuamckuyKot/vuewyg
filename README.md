# Vuewyg
[![vue2](https://img.shields.io/badge/vue-2.x-brightgreen.svg)](https://vuejs.org/)

Vue.JS based WYSIWYG blocks editor.

Build with:
* [Vue](https://vuejs.org/)	
* [Vuex](https://vuex.vuejs.org/ru/intro.html)
* [Vue-resource](https://github.com/pagekit/vue-resource)
* [Vuetify](https://github.com/vuetifyjs/vuetify)

Works with blocks as JSON-object, allows to import and export editor state.

## Installation

``` bash
# install via npm
npm install vuewyg --save
# or yarn
yarn add vuewyg --save 
```

You also need to install dependencies to get it work.
``` bash
npm install vuex vue-resource vuetify --save
# or
yarn add vuex vue-resource vuetify --save
```
## Usage
### index.js
Entry point script:
``` javascript
// if isn't imported yet
import Vue from 'vue'; 
import Vuex from 'vuex';
import App from './App';
import {Store} from 'vuewyg';
import Vuetify from 'vuetify';

Vue.use(Vuex);
Vue.use(Vuetify);

const store = new Vuex.Store(Store);

new Vue({
	el: '#app',
	store,
	components: { App },
	template: '<App/>'
	});
// or you can use alternate syntax:
new Vue({
	store,
	render: (h) => h(App)
	}).$mount('#app');
```


### App.vue
Root single file component in Vue:
``` vue
<template>
		<div id="app">
		<Editor/>
	</div>
</template>

<script>
	import {Editor} from 'vuewyg'
	export default {
		name: 'App',
		components: {Editor},
	}
</script>
<style>
</style>
```

### Editor state
By default you will see basic structure of blocks.
You need to import your own state as it going.
It can be done with ease via Vuex:
``` javascript
const store = new Vuex.Store(Store)

store.dispatch('ALL', {
	blocks: [
		{
			type: 'header',
			content: "Basic header",
			permamnent: true,
			settings: false
		},
		{
			type: 'paragraph',
			content: "Simple paragraph",
			permanent: true,
			settings: {}
		}
	]
})
new Vue({
	store,
	render: (h) => h(App)
}).$mount('#app')
```

#### State structure

State consists of:
* `blocks` **Array** Array of blocks (see the description below).
* `blockSettings` **Object** Adjustable settings for all blocks and for specific block types.

##### blocks
``` javascript
blocks: [
	{
		type: 'paragraph', // String (Required). Available by default: header, paragraph, link, video, image, blockquote, code.
		// Further block types can be added later by npm packages. See source package `/src/components/Editor.vue` for specification.
		content: '', // String (Optional). Content storage, which syncs with blocks content.
		permanent: true, // Boolean (Optional). Makes block undeletable by user.
		settings: {}, // Object (Optoional). Block settings storage.
		// Other fields are specific for custom blocks.
		// Like Video and Image blocks can has `src` and `caption` fields.
	}
]
```
##### blockSettings
These one is optional and is used to render and store popup `settingsModal` window. 
It's like a template of settings to be rendered for blocks. Real data is saved in `settings` field for every block.
``` javascript
blockSettings: {
	all: { // For all blocks. 
		inTeaser: {
			type: SETTINGS.TYPES.CHECKBOX, // See types in `/src/config.coffee`
			default: false, // Default value
			label: "Show in Teaser", // Label in modal settings window
		}
	},
	paragraph: { // Only for `paragraph` block type.
		someParam: {
			type: '',
			default: '', 
			label: '',
		}
	}
}
```

## Default URLs
`Editor` component has prop `options` (Object), which has `url` property (Object).

The default value of `options` prop is:
``` javascript
{
	url: {
		image_upload: '/static/upload_image.json',
		og: 'http://localhost:9999?url='
	}
}
```
Keys are:
* `image_upload` — path for upload image (POST, file with name `image`).
* `og` — OpenGraph server which gives JSON-o	bject in response.

Those need to be specified in case to make your own logic.
