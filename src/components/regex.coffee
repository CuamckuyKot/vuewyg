export REGEX =
	YOUTUBE: /https?:\/\/(?:www\.)?youtu(?:\.be\/|be\.com\/watch\?v=)([\w_-]{11})$/
	TWITTER: /https?:\/\/twitter.com\/[\w_-]+\/status\/(\d+)\/?$/
	IMAGE: /https?:\/\/.+\.(jpe?g|gif|png)$/
	LINK: /https?:\/\/[^\s]+/
