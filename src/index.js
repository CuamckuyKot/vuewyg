import Editor from './components/Editor.vue'
import Store from './vuex.coffee' 
import {finder} from './mixins/finder.coffee'
// export function install (Vue) {
// 	/* -- Add more components here -- */
// }

// Expose the components
export {
	Editor,
	finder,
	Store  /* -- Add more components here -- */
}

/* -- Plugin definition & Auto-install -- */
/* You shouldn't have to modify the code below */

// Plugin
// const plugin = {
// 	/* eslint-disable no-undef */
// 	version:'1.0.0',
// 	install,
// }

export default Editor

// Auto-install
let GlobalVue = null
if (typeof window !== 'undefined') {
	GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
	GlobalVue = global.Vue
}
if (GlobalVue) {
	// GlobalVue.use(plugin)
	GlobalVue.mixin(finder)
}
