import Vue from 'vue'
import arrayMove from 'array-move'
export default
	namespaced: true
	state:
		blocks: []
		blockSettings: {}
		selectedBlockIndex: null
		validators: {}
	getters: 
		export: (state) -> state
		getValidators: (state) -> (type) ->
			if state.validators?[type]?
				return state.validators[type]
		getSettings: (state) -> (type) ->
			if type?
				if state.blockSettings?.all
					return Object.assign {}, state.blockSettings.all, state.blockSettings[type]
				else
					state.blockSettings?[type]
			state.blockSettings?.all
		getBlocks: (state) ->
			state.blocks
		getBlock: (state) -> (id) ->
			blocks = state.blocks.filter (block) -> block.id == id
			if blocks then blocks[0]  else null
		getBlockIndex: (state) -> (id) ->
			state.blocks.findIndex (block) => block.id == id
		getSelectedBlock: (state) ->
			if state.selectedBlockIndex isnt null
				return state.blocks[state.selectedBlockIndex]
			null
		getBlockBy: (state) -> (param,value) ->
			block = state.blocks.filter (block) -> block[param] == value
			return !!block.length
		getNextBlock: (state) ->
			state.blocks[state.selectedBlockIndex+1]
		getPrevBlock: (state) ->
			state.blocks[state.selectedBlockIndex-1]
		getSelectedBlockIndex: (state) ->
			state.selectedBlockIndex
	mutations: 
		all: (state, newState) ->
			Vue.set state,'blocks',newState.blocks if newState.blocks?
			Vue.set state,'blockSettings',newState.blockSettings if newState.blockSettings?
			Vue.set state,'validators',newState.validators if newState.validators?
		blocks: (state, blocks) ->
			Vue.set state,'blocks',blocks
		select: (state, index) ->
			Vue.set state,'selectedBlockIndex',index
		add: (state,block) ->
			if null == state.selectedBlockIndex || 1 > state.selectedBlockIndex || state.selectedBlockIndex == (state.blocks.length - 1)
				Vue.set state.blocks,state.blocks.length,block
			else
				state.blocks.splice(state.selectedBlockIndex+1,0,block)
				state.blocks = [].concat(state.blocks)
		replace: (state, block) ->
			state.blocks.splice(state.selectedBlockIndex,0,block)
			state.blocks = [].concat(state.blocks)	
				#Vue.set state,'selectedBlockIndex',state.selectedBlockIndex
		update: (state,options) ->
			{ index } = options
			delete options.index
			delete options.id
			options =Object.assign {}, state.blocks[index], options
			Vue.set state.blocks,index,options
		destroy: (state,index) ->
			state.blocks = state.blocks.filter (b,i) -> i isnt index
		moveUp: (state,index) ->
		#	console.log 'mutation/moveup',state.blocks,(arrayMove state.blocks, index, index-1)
			Vue.set state,'blocks',(arrayMove state.blocks, index, index-1)
		moveDown: (state,index) ->
		#	console.log 'mutation/movedown'
			Vue.set state,'blocks',(arrayMove state.blocks, index, index+1)
	actions:
		all: ({commit},state)->
			commit('all',state)
		blocks: ({commit}, blocks) ->
			commit('blocks',blocks)
		select: ({state, commit, getters}, id) ->
			blockIndex = getters.getBlockIndex(id)
			commit('select',blockIndex)
		add: ({commit,getters},options = {}) ->
			# s = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
			# id = Array(10).join().split(',').map(() -> s.charAt(Math.floor(Math.random() * s.length))).join('')
			id = new Date().getTime()
			defBlock =
				id: 'block-'+id,
				content: ""
				settings: {}
			block = Object.assign({},defBlock, options)
			new Promise (resolve,reject) ->
				commit('add',block)
				commit('select',getters.getBlockIndex(block.id))
				resolve(block)
		replace: ({commit,getters},options = {}) ->
			id = new Date().getTime()
			defBlock =
				id: 'block-'+id,
				content: ""
				settings: {}
			block = Object.assign({},defBlock, options)
			new Promise (resolve,reject) ->
				commit('replace',block)
				#commit('select',getters.getBlockIndex(block.id))
				resolve(block)
		update: ({commit, getters}, options = {}) ->
			options.index = getters.getBlockIndex(options.id)
			commit('update',options)
		destroy: ({state, commit, getters}, options = {}) ->
			unless options.id?
				block = getters.getSelectedBlock
				options.id = block.id
			index = getters.getBlockIndex(options.id) 
			if options.select_previous && index > 0
				commit('select',index - 1)
			commit('destroy',index)
		moveUp: ({commit,getters},id) ->
			#console.log 'action/moveup'
			index = getters.getBlockIndex id
			commit 'moveUp',index
		moveDown: ({commit,getters},id) ->
			#console.log 'action/movedown'
			index = getters.getBlockIndex id
			commit 'moveDown',index
