import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import App from './App'
import data from './vuex' 
import {finder} from './mixins/finder.coffee'
import 'vuetify/dist/vuetify.min.css'
import axios from 'axios'
#if token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
# axios.defaults.headers.common['X-CSRF-Token'] = token
#	axios.defaults.headers.common['Accept'] = 'application/json'

window.axios = axios

Vue.config.productionTip = false
Vue.use Vuex
Vue.use VueResource
Vue.use Vuetify
Vue.mixin finder
config =
	modules:
		vuewyg: data
store = new Vuex.Store config
store.dispatch 'vuewyg/all', {
	blocks: [
		{
			id: "header_1"
			type: "header"
			content: "Article header"
			placeholder: "Заголовок материала…"
			permanent: true
		}
		{
			id: "p_1"
			type: "paragraph"
			content: "There is some text here."
			placeholder: "Вступительный текст…"
			permanent: true
			settings: false
		}		
		{
			id: "image-1"
			type: "image"
			align: 1
			size: 3
			src: "http://localhost:3000/uploads/images/1/2018/3/10/771ffe.jpg"
			image:{"file":{"full":"http://localhost:3000/uploads/images/1/2018/3/10/771ffe.jpg","large":{"url":"http://localhost:3000/uploads/images/1/2018/3/10/771ffe_post.jpg"},"comment":{"url":"http://localhost:3000/uploads/images/1/2018/3/10/771ffe_comment.jpg"},"medium":{"url":"http://localhost:3000/uploads/images/1/2018/3/10/771ffe_medium.jpg"},"small":{"url":"http://localhost:3000/uploads/images/1/2018/3/10/771ffe_thumb.jpg"}}}
		}	
		{
			id: "video-1"
			type: 'video'
			src: 'https://youtu.be/ZKSQrLbTBkg'
		}
	]
	# blockSettings:
	# 	all:
	# 		inTeaser:
	# 			type: 'checkbox'
	# 			default: false
	# 			label: "Отображать в тизере"
	validators:
		header: 
			content: (v,block) -> 
				if block.id == 'header_1'
					'Заголовок не может быть пустым' if v.length < 1 or v.match /^(&nbsp;|\s)+$/ 
		paragraph:
			content: (v,block) ->
				if block.id == 'p_1'
					'Вступительный текст не может быть пустым' if v.length < 1 or v.match /^(&nbsp;|\s)+$/ 
}
window.Vue = Vue
window.editor = new Vue
	store: store,
	render: (h) =>	h(App)
.$mount('#vuewyg')
