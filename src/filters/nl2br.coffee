export default nl2br = (value, reg = /\n\r/g) =>
	if value?
		s = ''
		lines = value.split(reg)
		len = lines.length;
		for line, i in lines
			s += line
			s += "<br/>"
		return s
	value
