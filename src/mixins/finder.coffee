export finder =
	methods: 
		parent: (name='') ->
			parent = @$parent
			if name
				while parent
					return parent if parent.$options.name == name
					parent = parent.$parent

